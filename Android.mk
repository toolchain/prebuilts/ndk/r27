# Intentionally empty to prevent loading subdir Android.mk files.
# The distributed NDK includes a handful of Android.mk files for use
# with ndk-build via import-module, but without an empty Android.mk at
# the top level, the platform build system will try to use them.
